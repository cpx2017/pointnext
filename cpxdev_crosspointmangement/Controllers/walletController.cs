﻿using ByteDev.Encoding.Base32;
using ByteDev.Encoding.Base64;
using cpxdev_crosspointmangement.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using Newtonsoft.Json;
using System.Text.Json;
using System.Text.RegularExpressions;

namespace cpxdev_crosspointmangement.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class walletController : ControllerBase
    {
        [HttpGet("{walletCode}")]
        public JsonResult getWallet([FromRoute] string walletCode)
        {
            if (walletCode == null || walletCode == "")
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Wallet Code is required."
                });
            }
            walletForm setobj = new walletForm();
            for (int i = 1; i < 4; i++)
            {
                MongoClient dbclient = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{new Zoneselect().zoneselection(i).Split(",")[0]}.mongodb.net/");
                var db = dbclient.GetDatabase("main");
                var coll = db.GetCollection<BsonDocument>("user");
                var projection = Builders<BsonDocument>.Projection.Exclude("_id").Exclude("passKey");
                var filter = Builders<BsonDocument>.Filter.Eq("walletCode", walletCode);
                if (coll.Find(filter).Count() > 0)
                {
                    var obj = coll.Find(filter).Project(projection).FirstOrDefault();
                    setobj = BsonSerializer.Deserialize<walletForm>(obj);
                    break;
                }
            }
            if (setobj.walletCode == null)
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Wallet is not found."
                });
            }

            return new JsonResult(new
            {
                status = true,
                responses = setobj
            });
        }

        [HttpPost]
        public JsonResult createWallet([FromBody] CreateWalletModel req)
        {
            if (req.zone == 0)
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Zone is required"
                });
            }
            if (req.passKey == "")
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Pass Key is required"
                });
            }
            string region = new Zoneselect().zoneselection(req.zone);
            if (region == "")
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Zone is unavaliable"
                });
            }

            var start = DateTime.UtcNow;
            string id = Guid.NewGuid().ToString();
            string walletCodeplain = JsonConvert.SerializeObject(new
            {
                walletId = id,
                createdDate = start,
                zoneNumber = req.zone,
                regionZone = region.Split(",")[2]
            });
            walletCodeplain = new Zoneselect().MD5Hash(walletCodeplain);

            MongoClient dbclient = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{region.Split(",")[0]}.mongodb.net/");
            var db = dbclient.GetDatabase("main");
            var coll = db.GetCollection<BsonDocument>("user");
            coll.InsertOne(new
            {
                walletCode = walletCodeplain,
                passKey = new Zoneselect().MD5Hash(new Base32Encoder().Encode(req.passKey)),
                createdDate = start,
                receivedWallet = req.receivedWallet,
                pointState = 0.0000,
                zoneNumber = req.zone,
                regionZone = region.Split(",")[2]
            }.ToBsonDocument());

            string tran = new Zoneselect().MD5Hash(DateTime.UtcNow.ToString("s") + Guid.NewGuid().ToString() + Guid.NewGuid().ToString());

            transactionForm objTran = new transactionForm();
            objTran.fromWalletCode = "";
            objTran.toWalletCode = walletCodeplain;
            objTran.transactionId = tran;
            objTran.pointEarn = 0;
            objTran.removeEnt = DateTime.UtcNow;
            objTran.to = new Zoning()
            {
                zoneNumber = req.zone,
                regionZone = new Zoneselect().zoneselection(req.zone).Split(",")[2]
            };
            objTran.text = new Base64Encoder().Encode("Create Wallet" + (req.receivedWallet == true ? " (Received wallet only)" : ""));


            Task.Run(() =>
            {
                object obj = new Zoneselect().sendLogNonSign(objTran, req.zone);
                new Zoneselect().sendLogwithSign(objTran, req.zone);
            });

            return new JsonResult(new
            {
                status = true,
                walletCode = walletCodeplain,
                transactionId = tran
            });
        }

        [HttpDelete]
        public JsonResult deleteWallet([FromBody] DeleteWalletModel req)
        {
            if (req.passKey == "")
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Pass Key is required"
                });
            }
            var start = DateTime.UtcNow;
            string id = Guid.NewGuid().ToString();

            walletForm setobj = new walletForm();
            for (int i = 1; i < 4; i++)
            {
                MongoClient dbclient = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{new Zoneselect().zoneselection(i).Split(",")[0]}.mongodb.net/");
                var db = dbclient.GetDatabase("main");
                var coll = db.GetCollection<BsonDocument>("user");
                var projection = Builders<BsonDocument>.Projection.Exclude("_id");
                var filter = Builders<BsonDocument>.Filter.Eq("walletCode", req.walletCode) & Builders<BsonDocument>.Filter.Eq("passKey", new Zoneselect().MD5Hash(new Base32Encoder().Encode(req.passKey)));
                if (coll.Find(filter).Count() > 0)
                {
                    var obj = coll.Find(filter).Project(projection).FirstOrDefault();
                    setobj = BsonSerializer.Deserialize<walletForm>(obj);
                    break;
                }
            }
            if (setobj.walletCode == null)
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Wallet is already deleted."
                });
            }
            if (setobj.passKey != new Zoneselect().MD5Hash(new Base32Encoder().Encode(req.passKey)))
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Pass Key is incorrect."
                });
            }

            MongoClient dbx = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{new Zoneselect().zoneselection(setobj.zoneNumber).Split(",")[0]}.mongodb.net/");
            var db1 = dbx.GetDatabase("main");
            var coll1 = db1.GetCollection<BsonDocument>("user");
            var filter1 = Builders<BsonDocument>.Filter.Eq("walletCode", req.walletCode) & Builders<BsonDocument>.Filter.Eq("passKey", setobj.passKey);
            var o = coll1.DeleteOne(filter1);

            string tran = new Zoneselect().MD5Hash(DateTime.UtcNow.ToString("s") + Guid.NewGuid().ToString() + Guid.NewGuid().ToString());

            transactionForm objTran = new transactionForm();
            objTran.fromWalletCode = req.walletCode;
            objTran.toWalletCode = "";
            objTran.transactionId = tran;
            objTran.removeEnt = DateTime.UtcNow;
            objTran.from = new Zoning()
            {
                zoneNumber = setobj.zoneNumber,
                regionZone = new Zoneselect().zoneselection(setobj.zoneNumber).Split(",")[2]
            };
            objTran.text = new Base64Encoder().Encode("Delete Wallet");

            Task.Run(() =>
            {
                object obj = new Zoneselect().sendLogNonSign(objTran, setobj.zoneNumber);
                new Zoneselect().sendLogwithSign(objTran, setobj.zoneNumber);
            });

            return new JsonResult(new
            {
                status = true,
                walletCode = req.walletCode,
                transactionId = tran
            });
        }

        [HttpDelete]
        public bool Clearalltranaction()
        {
            for (int i = 1; i < 4; i++)
            {
                MongoClient dbclient = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{new Zoneselect().zoneselection(i).Split(",")[0]}.mongodb.net/");
                var db = dbclient.GetDatabase("main");
                var coll = db.GetCollection<BsonDocument>("transaction");
                coll.DeleteMany(new BsonDocument());
            }
            return true;
        }
    }
}
