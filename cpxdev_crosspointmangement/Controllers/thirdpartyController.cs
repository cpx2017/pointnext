﻿using cpxdev_crosspointmangement.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson.Serialization;
using MongoDB.Bson;
using MongoDB.Driver;
using ByteDev.Encoding.Base64;

namespace cpxdev_crosspointmangement.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class thirdpartyController : ControllerBase
    {

        [HttpPut]
        public JsonResult withdrawalTransaction([FromBody] CreateTransactionWithThird req)
        {
            if (req.toWalletName == "")
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Target Wallet Name is required."
                });
            }
            if (req.point < 0)
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Point parameter is required."
                });
            }

            if (req.feePoint > req.point)
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Point Fee parameter should be less than earned points."
                });
            }

            walletForm ase1 = new walletForm();
            for (int i = 1; i < 4; i++)
            {
                MongoClient dbclient1 = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{new Zoneselect().zoneselection(i).Split(",")[0]}.mongodb.net/");
                var db1 = dbclient1.GetDatabase("main");
                var coll1 = db1.GetCollection<BsonDocument>("user");
                var projection1 = Builders<BsonDocument>.Projection.Exclude("_id");
                var filter1 = Builders<BsonDocument>.Filter.Eq("walletCode", req.fromWalletCode);
                if (coll1.Find(filter1).Count() > 0)
                {
                    var obj = coll1.Find(filter1).Project(projection1).FirstOrDefault();
                    ase1 = BsonSerializer.Deserialize<walletForm>(obj);
                    break;
                }
            }
            if (ase1.walletCode == null)
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Wallet is not found."
                });
            }
            if (ase1.receivedWallet)
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "You cannot transfer points to another wallet. But you can destroy point of this origin wallet."
                });
            }

            string tran = new Zoneselect().MD5Hash(DateTime.UtcNow.ToString("s") + Guid.NewGuid().ToString() + Guid.NewGuid().ToString());
            transactionForm objTran = new transactionForm();
            objTran.fromWalletCode = ase1.walletCode;
            objTran.toWalletCode = "";
            objTran.transactionId = tran;
            objTran.pointFee = req.feePoint;
            objTran.pointEarn = req.point;
            objTran.dateChanged = DateTime.UtcNow;
            objTran.removeEnt = DateTime.UtcNow;
            objTran.from = new Zoning()
            {
                zoneNumber = ase1.zoneNumber,
                regionZone = new Zoneselect().zoneselection(ase1.zoneNumber).Split(",")[2]
            };
            objTran.text = new Base64Encoder().Encode("Create Transaction from " + ase1.walletCode + " to Third Party wallet (" + req.toWalletName + ") by " + req.point + " points");

            double pointset = req.point - req.feePoint;

            MongoClient dbclient = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{new Zoneselect().zoneselection(ase1.zoneNumber).Split(",")[0]}.mongodb.net/");
            var db = dbclient.GetDatabase("main");
            var coll = db.GetCollection<BsonDocument>("user");

            new Zoneselect().sendLogNonSign(objTran, ase1.zoneNumber);
            var filterc1 = Builders<BsonDocument>.Filter.Eq("walletCode", req.fromWalletCode) & Builders<BsonDocument>.Filter.Gte("pointState", req.point);
            var set = coll.UpdateOne(filterc1, Builders<BsonDocument>.Update.Inc("pointState", objTran.pointEarn - (objTran.pointEarn * 2)));
            if (set.ModifiedCount == 0)
            {
                coll.UpdateOne(Builders<BsonDocument>.Filter.Eq("walletCode", req.fromWalletCode), Builders<BsonDocument>.Update.Set("pointState", 0));
            }
            Task.Run(() =>
            {
                if (new Zoneselect().listVerify(tran) == false)
                {
                    new Zoneselect().sendLogwithSign(objTran, ase1.zoneNumber);
                }
            });

            return new JsonResult(new
            {
                status = true,
                transaction = objTran
            });
        }

        [HttpPut]
        public JsonResult earnTranaction([FromBody] ReceivedPointTransactionWithThird req)
        {
            if (req.fromWalletName == "")
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Target Wallet Name is required."
                });
            }
            if (req.point <= 0)
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Point parameter is required."
                });
            }

            walletForm ase1 = new walletForm();
            for (int i = 1; i < 4; i++)
            {
                MongoClient dbclient1 = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{new Zoneselect().zoneselection(i).Split(",")[0]}.mongodb.net/");
                var db1 = dbclient1.GetDatabase("main");
                var coll1 = db1.GetCollection<BsonDocument>("user");
                var projection1 = Builders<BsonDocument>.Projection.Exclude("_id");
                var filter1 = Builders<BsonDocument>.Filter.Eq("walletCode", req.toWalletCode);
                if (coll1.Find(filter1).Count() > 0)
                {
                    var obj = coll1.Find(filter1).Project(projection1).FirstOrDefault();
                    ase1 = BsonSerializer.Deserialize<walletForm>(obj);
                    break;
                }
            }
            if (ase1.walletCode == null)
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Wallet is not found."
                });
            }

            string tran = new Zoneselect().MD5Hash(DateTime.UtcNow.ToString("s") + Guid.NewGuid().ToString() + Guid.NewGuid().ToString());
            transactionForm objTran = new transactionForm();
            objTran.fromWalletCode = "";
            objTran.toWalletCode = req.toWalletCode;
            objTran.transactionId = tran;
            objTran.pointFee = 0;
            objTran.pointEarn = req.point;
            objTran.dateChanged = DateTime.UtcNow;
            objTran.removeEnt = DateTime.UtcNow;
            objTran.text = new Base64Encoder().Encode("Create Transaction from Third Party wallet (" + req.fromWalletName + ") to " + ase1.walletCode + " by " + req.point + " points");

            double pointset = req.point - 0;

            MongoClient dbclient = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{new Zoneselect().zoneselection(ase1.zoneNumber).Split(",")[0]}.mongodb.net/");
            var db = dbclient.GetDatabase("main");
            var coll = db.GetCollection<BsonDocument>("user");

            new Zoneselect().sendLogNonSign(objTran, ase1.zoneNumber);
            var filterc1 = Builders<BsonDocument>.Filter.Eq("walletCode", req.toWalletCode);
            var set = coll.UpdateOne(filterc1, Builders<BsonDocument>.Update.Inc("pointState", pointset));
            Task.Run(() =>
            {
                if (new Zoneselect().listVerify(tran) == false)
                {
                    new Zoneselect().sendLogwithSign(objTran, ase1.zoneNumber);
                }
            });

            return new JsonResult(new
            {
                status = true,
                transaction = objTran
            });
        }
    }
}
