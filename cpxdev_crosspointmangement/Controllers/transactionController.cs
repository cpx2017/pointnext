﻿using cpxdev_crosspointmangement.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson.Serialization;
using MongoDB.Bson;
using MongoDB.Driver;
using LazyCache;
using ByteDev.Encoding.Base64;

namespace cpxdev_crosspointmangement.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class transactionController : ControllerBase
    {
        private readonly IAppCache _cache;

        public transactionController(IAppCache appCache)
        {
            _cache = appCache;
        }

        [HttpGet("{walletCode}")]
        public JsonResult listTransaction([FromRoute] string walletCode)
        {
            if (walletCode == null || walletCode == "")
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Wallet Code is required."
                });
            }

            walletForm ase = new walletForm();
            for (int i = 1; i < 4; i++)
            {
                MongoClient dbclient1 = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{new Zoneselect().zoneselection(i).Split(",")[0]}.mongodb.net/");
                var db1 = dbclient1.GetDatabase("main");
                var coll1 = db1.GetCollection<BsonDocument>("user");
                var projection1 = Builders<BsonDocument>.Projection.Exclude("_id");
                var filter1 = Builders<BsonDocument>.Filter.Eq("walletCode", walletCode);
                if (coll1.Find(filter1).Count() > 0)
                {
                    var obj = coll1.Find(filter1).Project(projection1).FirstOrDefault();
                    ase = BsonSerializer.Deserialize<walletForm>(obj);
                    break;
                }
            }
            if (ase.walletCode == null)
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Wallet is not found."
                });
            }

            List<transaction> setobj = new List<transaction>();
            for (int i = 1; i < 4; i++)
            {
                MongoClient dbclient = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{new Zoneselect().zoneselection(i).Split(",")[0]}.mongodb.net/");
                var db = dbclient.GetDatabase("main");
                var coll = db.GetCollection<BsonDocument>("transaction");
                var projection = Builders<BsonDocument>.Projection.Exclude("removeEnt").Exclude("_id");
                var filter = Builders<BsonDocument>.Filter.Eq("fromWalletCode", walletCode) | Builders<BsonDocument>.Filter.Eq("toWalletCode", walletCode);
                foreach (var item in coll.Find(filter).Project(projection).Sort(Builders<BsonDocument>.Sort.Descending("dateChanged")).ToList())
                {
                    transaction o = BsonSerializer.Deserialize<transaction>(item);
                    if (_cache.GetAsync<int>("trans-" + o.transactionId).Result == 0)
                    {
                        bool signx = new Zoneselect().listVerify(o.transactionId);
                        _cache.Add("trans-" + o.transactionId, signx == true ? 2 : 1, signx == true ? DateTimeOffset.Now.AddMinutes(30) : DateTimeOffset.Now.AddMinutes(1));
                        o.signed = signx;
                    }
                    else
                    {
                        o.signed = _cache.GetAsync<int>("trans-" + o.transactionId).Result == 2 ? true : false;
                    }
                    if (setobj.FindIndex(x => x.transactionId == o.transactionId) == -1)
                    {
                        setobj.Add(o);
                    }
                }
            }

            if (setobj.Count == 0)
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Transaction are not found."
                });
            }

            return new JsonResult(new
            {
                status = true,
                walletCode = walletCode,
                responses = setobj
            });
        }

        [HttpGet("{walletCode}/{transactionId}")]
        public JsonResult getTransaction([FromRoute] string walletCode, [FromRoute] string transactionId)
        {
            if (walletCode == null || walletCode == "")
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Wallet Code is required."
                });
            }

            walletForm ase = new walletForm();
            for (int i = 1; i < 4; i++)
            {
                MongoClient dbclient1 = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{new Zoneselect().zoneselection(i).Split(",")[0]}.mongodb.net/");
                var db1 = dbclient1.GetDatabase("main");
                var coll1 = db1.GetCollection<BsonDocument>("user");
                var projection1 = Builders<BsonDocument>.Projection.Exclude("_id");
                var filter1 = Builders<BsonDocument>.Filter.Eq("walletCode", walletCode);
                if (coll1.Find(filter1).Count() > 0)
                {
                    var obj = coll1.Find(filter1).Project(projection1).FirstOrDefault();
                    ase = BsonSerializer.Deserialize<walletForm>(obj);
                    break;
                }
            }
            if (ase.walletCode == null)
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Wallet is not found."
                });
            }

            transaction setobj = new transaction();
            MongoClient dbclient = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{new Zoneselect().zoneselection(ase.zoneNumber).Split(",")[0]}.mongodb.net/");
            var db = dbclient.GetDatabase("main");
            var coll = db.GetCollection<BsonDocument>("transaction");
            var projection = Builders<BsonDocument>.Projection.Exclude("removeEnt").Exclude("_id");
            var filter = (Builders<BsonDocument>.Filter.Eq("fromWalletCode", walletCode) | Builders<BsonDocument>.Filter.Eq("toWalletCode", walletCode)) & Builders<BsonDocument>.Filter.Eq("transactionId", transactionId);

            var aa = coll.Find(filter).Project(projection).FirstOrDefault();
            if (aa == null)
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "This transaction is not found."
                });
            }
            setobj = BsonSerializer.Deserialize<transaction>(aa);
            if (_cache.GetAsync<int>("xtrans-" + setobj.transactionId).Result == 0)
            {
                bool signx = new Zoneselect().listVerify(setobj.transactionId);
                _cache.Add("xtrans-" + setobj.transactionId, signx == true ? 2 : 1, DateTimeOffset.Now.AddMinutes(1));
                setobj.signed = signx;
            }
            else
            {
                setobj.signed = _cache.GetAsync<int>("xtrans-" + setobj.transactionId).Result == 2 ? true : false;
            }

            if (setobj.transactionId == null)
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Transaction are not found."
                });
            }

            return new JsonResult(new
            {
                status = true,
                walletCode = walletCode,
                responses = setobj
            });
        }

        [HttpPost]
        public JsonResult createTransaction([FromBody] CreateTransaction req)
        {
            if (req.fromWalletCode == "" || req.toWalletCode == "")
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Wallet Code is required."
                });
            }
            if (req.point <= 0)
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Point parameter is required."
                });
            }
            if (req.feePoint <= 0)
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Point Fee parameter is required for transfer point between another Region Zone."
                });
            }

            if (req.feePoint > req.point)
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Point Fee parameter should be less than earned points."
                });
            }

            walletForm ase1 = new walletForm();
            for (int i = 1; i < 4; i++)
            {
                MongoClient dbclient1 = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{new Zoneselect().zoneselection(i).Split(",")[0]}.mongodb.net/");
                var db1 = dbclient1.GetDatabase("main");
                var coll1 = db1.GetCollection<BsonDocument>("user");
                var projection1 = Builders<BsonDocument>.Projection.Exclude("_id");
                var filter1 = Builders<BsonDocument>.Filter.Eq("walletCode", req.fromWalletCode);
                if (coll1.Find(filter1).Count() > 0)
                {
                    var obj = coll1.Find(filter1).Project(projection1).FirstOrDefault();
                    ase1 = BsonSerializer.Deserialize<walletForm>(obj);
                    break;
                }
            }
            if (ase1.walletCode == null)
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Wallet is not found."
                });
            }
            if (ase1.receivedWallet)
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "You cannot transfer points to another wallet. But you can destroy point of this origin wallet."
                });
            }

            walletForm ase2 = new walletForm();
            for (int i = 1; i < 4; i++)
            {
                MongoClient dbclient1 = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{new Zoneselect().zoneselection(i).Split(",")[0]}.mongodb.net/");
                var db1 = dbclient1.GetDatabase("main");
                var coll1 = db1.GetCollection<BsonDocument>("user");
                var projection1 = Builders<BsonDocument>.Projection.Exclude("_id");
                var filter1 = Builders<BsonDocument>.Filter.Eq("walletCode", req.toWalletCode);
                if (coll1.Find(filter1).Count() > 0)
                {
                    var obj = coll1.Find(filter1).Project(projection1).FirstOrDefault();
                    ase2 = BsonSerializer.Deserialize<walletForm>(obj);
                    break;
                }
            }
            if (ase2.walletCode == null)
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Wallet is not found."
                });
            }

            string tran = new Zoneselect().MD5Hash(DateTime.UtcNow.ToString("s") + Guid.NewGuid().ToString() + Guid.NewGuid().ToString());
            transactionForm objTran = new transactionForm();
            objTran.fromWalletCode = ase1.walletCode;
            objTran.toWalletCode = ase2.walletCode;
            objTran.transactionId = tran;
            objTran.pointFee = req.feePoint;
            objTran.pointEarn = req.point;
            objTran.dateChanged = DateTime.UtcNow;
            objTran.removeEnt = DateTime.UtcNow;
            objTran.from = new Zoning()
            {
                zoneNumber = ase1.zoneNumber,
                regionZone = new Zoneselect().zoneselection(ase1.zoneNumber).Split(",")[2]
            };
            objTran.to = new Zoning()
            {
                zoneNumber = ase2.zoneNumber,
                regionZone = new Zoneselect().zoneselection(ase2.zoneNumber).Split(",")[2]
            };
            objTran.text = new Base64Encoder().Encode("Create Transaction from " + ase1.walletCode + " to " + ase2.walletCode + " by " + req.point + " points");

            double pointset = req.point - (ase1.zoneNumber != ase2.zoneNumber ? req.feePoint : 0);

            MongoClient dbclient = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{new Zoneselect().zoneselection(ase1.zoneNumber).Split(",")[0]}.mongodb.net/");
            var db = dbclient.GetDatabase("main");
            MongoClient dbclient11 = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{new Zoneselect().zoneselection(ase2.zoneNumber).Split(",")[0]}.mongodb.net/");
            var db11 = dbclient11.GetDatabase("main");
            var coll = db.GetCollection<BsonDocument>("user");
            var coll11 = db11.GetCollection<BsonDocument>("user");

            new Zoneselect().sendLogNonSign(objTran, ase1.zoneNumber);
            var filterc1 = Builders<BsonDocument>.Filter.Eq("walletCode", req.fromWalletCode) & Builders<BsonDocument>.Filter.Gte("pointState", req.point);
            var filterc2 = Builders<BsonDocument>.Filter.Eq("walletCode", req.toWalletCode);
            var set = coll.UpdateOne(filterc1, Builders<BsonDocument>.Update.Inc("pointState", objTran.pointEarn - (objTran.pointEarn * 2)));
            if (set.ModifiedCount == 0)
            {
                coll.UpdateOne(Builders<BsonDocument>.Filter.Eq("walletCode", req.fromWalletCode), Builders<BsonDocument>.Update.Set("pointState", 0));
            }
            if (req.autoSignedTransaction)
            {
                Task.Run(() =>
                {
                    Thread.Sleep(5000);
                    objTran.dateChanged = DateTime.UtcNow;
                    coll11.UpdateOne(filterc2, Builders<BsonDocument>.Update.Inc("pointState", pointset));
                    if (new Zoneselect().listVerify(tran) == false)
                    {
                        new Zoneselect().sendLogwithSign(objTran, ase1.zoneNumber);
                    }
                });
            }

            return new JsonResult(new
            {
                status = true,
                autoSigned = req.autoSignedTransaction,
                transaction = objTran
            });
        }

        [HttpPatch("{transactionId}")]
        public JsonResult signedTransaction([FromRoute] string transactionId)
        {
            if (transactionId == "")
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Transaction ID is required."
                });
            }
            if (new Zoneselect().listVerify(transactionId) == true)
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "This Transaction is already verified."
                });
            }

            transactionForm trans = new transactionForm();
            for (int i = 1; i < 4; i++)
            {
                MongoClient dbclient1 = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{new Zoneselect().zoneselection(i).Split(",")[0]}.mongodb.net/");
                var db1 = dbclient1.GetDatabase("main");
                var coll1 = db1.GetCollection<BsonDocument>("transaction");
                var projection1 = Builders<BsonDocument>.Projection.Exclude("_id");
                var filter1 = Builders<BsonDocument>.Filter.Eq("transactionId", transactionId);
                if (coll1.Find(filter1).Count() > 0)
                {
                    var obj = coll1.Find(filter1).Project(projection1).FirstOrDefault();
                    trans = BsonSerializer.Deserialize<transactionForm>(obj);
                    break;
                }
            }
            if (trans.transactionId == null)
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "This Transaction is not found."
                });
            }

            walletForm ase1 = new walletForm();
            for (int i = 1; i < 4; i++)
            {
                MongoClient dbclient1 = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{new Zoneselect().zoneselection(i).Split(",")[0]}.mongodb.net/");
                var db1 = dbclient1.GetDatabase("main");
                var coll1 = db1.GetCollection<BsonDocument>("user");
                var projection1 = Builders<BsonDocument>.Projection.Exclude("_id");
                var filter1 = Builders<BsonDocument>.Filter.Eq("walletCode", trans.fromWalletCode);
                if (coll1.Find(filter1).Count() > 0)
                {
                    var obj = coll1.Find(filter1).Project(projection1).FirstOrDefault();
                    ase1 = BsonSerializer.Deserialize<walletForm>(obj);
                    break;
                }
            }
            if (ase1.walletCode == null)
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Wallet is not found."
                });
            }

            walletForm ase2 = new walletForm();
            for (int i = 1; i < 4; i++)
            {
                MongoClient dbclient1 = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{new Zoneselect().zoneselection(i).Split(",")[0]}.mongodb.net/");
                var db1 = dbclient1.GetDatabase("main");
                var coll1 = db1.GetCollection<BsonDocument>("user");
                var projection1 = Builders<BsonDocument>.Projection.Exclude("_id");
                var filter1 = Builders<BsonDocument>.Filter.Eq("walletCode", trans.toWalletCode);
                if (coll1.Find(filter1).Count() > 0)
                {
                    var obj = coll1.Find(filter1).Project(projection1).FirstOrDefault();
                    ase2 = BsonSerializer.Deserialize<walletForm>(obj);
                    break;
                }
            }
            if (ase2.walletCode == null)
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Wallet is not found."
                });
            }

            double pointset = trans.pointEarn - (ase1.zoneNumber != ase2.zoneNumber ? trans.pointFee : 0);

            transactionForm objTran = new transactionForm();
            objTran.fromWalletCode = ase1.walletCode;
            objTran.toWalletCode = ase2.walletCode;
            objTran.transactionId = trans.transactionId;
            objTran.pointFee = trans.pointFee;
            objTran.dateChanged = DateTime.UtcNow;
            objTran.pointEarn = trans.pointEarn;
            objTran.removeEnt = DateTime.UtcNow;
            objTran.dateChanged = DateTime.UtcNow;
            objTran.from = new Zoning()
            {
                zoneNumber = ase1.zoneNumber,
                regionZone = new Zoneselect().zoneselection(ase1.zoneNumber).Split(",")[2]
            };
            objTran.to = new Zoning()
            {
                zoneNumber = ase2.zoneNumber,
                regionZone = new Zoneselect().zoneselection(ase2.zoneNumber).Split(",")[2]
            };
            objTran.text = new Base64Encoder().Encode("Create Transaction from " + ase1.walletCode + " to " + ase2.walletCode + " by " + trans.pointEarn + " points");
            MongoClient dbclient11 = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{new Zoneselect().zoneselection(ase2.zoneNumber).Split(",")[0]}.mongodb.net/");
            var db11 = dbclient11.GetDatabase("main");
            var coll11 = db11.GetCollection<BsonDocument>("user");

            if (new Base64Encoder().Decode(trans.text).Contains("Destroy Wallet"))
            {
                new Zoneselect().sendLogNonSign(objTran, ase1.zoneNumber);
                var filterc2 = Builders<BsonDocument>.Filter.Eq("walletCode", trans.fromWalletCode);
                coll11.UpdateOne(filterc2, Builders<BsonDocument>.Update.Inc("pointState", 0));
                new Zoneselect().sendLogwithSign(objTran, ase1.zoneNumber);
            } else
            {
                new Zoneselect().sendLogNonSign(objTran, ase1.zoneNumber);
                var filterc2 = Builders<BsonDocument>.Filter.Eq("walletCode", trans.toWalletCode);
                coll11.UpdateOne(filterc2, Builders<BsonDocument>.Update.Inc("pointState", pointset));
                new Zoneselect().sendLogwithSign(objTran, ase1.zoneNumber);
            }

            return new JsonResult(new
            {
                status = true,
                transaction = objTran
            });
        }

        [HttpDelete("{transactionId}")]
        public JsonResult destroyTransaction([FromRoute] string walletCode, [FromBody] DestroyTransaction req)
        {
            if (walletCode == null)
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Wallet Code is required."
                });
            }
            if (walletCode == "")
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Wallet Code is required."
                });
            }

            walletForm ase1 = new walletForm();
            for (int i = 1; i < 4; i++)
            {
                MongoClient dbclient1 = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{new Zoneselect().zoneselection(i).Split(",")[0]}.mongodb.net/");
                var db1 = dbclient1.GetDatabase("main");
                var coll1 = db1.GetCollection<BsonDocument>("user");
                var projection1 = Builders<BsonDocument>.Projection.Exclude("_id");
                var filter1 = Builders<BsonDocument>.Filter.Eq("walletCode", walletCode);
                if (coll1.Find(filter1).Count() > 0)
                {
                    var obj = coll1.Find(filter1).Project(projection1).FirstOrDefault();
                    ase1 = BsonSerializer.Deserialize<walletForm>(obj);
                    break;
                }
            }
            if (ase1.walletCode == null)
            {
                return new JsonResult(new
                {
                    status = false,
                    message = "Wallet is not found."
                });
            }

            string tran = new Zoneselect().MD5Hash(DateTime.UtcNow.ToString("s") + Guid.NewGuid().ToString() + Guid.NewGuid().ToString());
            transactionForm objTran = new transactionForm();
            objTran.fromWalletCode = ase1.walletCode;
            objTran.toWalletCode = "00000000000000000";
            objTran.transactionId = tran;
            objTran.pointFee = 0;
            objTran.pointEarn = ase1.pointState;
            objTran.dateChanged = DateTime.UtcNow;
            objTran.removeEnt = DateTime.UtcNow;
            objTran.from = new Zoning()
            {
                zoneNumber = ase1.zoneNumber,
                regionZone = new Zoneselect().zoneselection(ase1.zoneNumber).Split(",")[2]
            };
            objTran.text = new Base64Encoder().Encode("Destroy Wallet in " + ase1.walletCode);

            double pointset = ase1.pointState - 0;

            MongoClient dbclient = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{new Zoneselect().zoneselection(ase1.zoneNumber).Split(",")[0]}.mongodb.net/");
            var db = dbclient.GetDatabase("main");
            var coll = db.GetCollection<BsonDocument>("user");

            new Zoneselect().sendLogNonSign(objTran, ase1.zoneNumber);
            var filterc1 = Builders<BsonDocument>.Filter.Eq("walletCode", walletCode);

            if (req.autoSignedTransaction)
            {
                coll.UpdateOne(filterc1, Builders<BsonDocument>.Update.Inc("pointState", 0));
                Task.Run(() =>
                {
                    Thread.Sleep(5000);
                    objTran.dateChanged = DateTime.UtcNow;
                    if (new Zoneselect().listVerify(tran) == false)
                    {
                        new Zoneselect().sendLogwithSign(objTran, ase1.zoneNumber);
                    }
                });
            }

            return new JsonResult(new
            {
                status = true,
                autoSigned = req.autoSignedTransaction,
                transaction = objTran
            });
        }
    }
}
