﻿using Amazon.Runtime.Internal.Util;
using cpxdev_crosspointmangement.Model;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson.Serialization;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Diagnostics;
using LazyCache;
using System.Transactions;

namespace cpxdev_crosspointmangement.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IAppCache _cache;

        public HomeController(ILogger<HomeController> logger, IAppCache appCache)
        {
            _logger = logger;
            _cache = appCache;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("home/TransactionList")]
        [HttpGet("home/TransactionList/{walletid}")]
        public IActionResult TransactionList([FromRoute] string walletid = "")
        {
            walletForm ase = new walletForm();
            for (int i = 1; i < 4; i++)
            {
                MongoClient dbclient1 = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{new Zoneselect().zoneselection(i).Split(",")[0]}.mongodb.net/");
                var db1 = dbclient1.GetDatabase("main");
                var coll1 = db1.GetCollection<BsonDocument>("user");
                var projection1 = Builders<BsonDocument>.Projection.Exclude("_id");
                var filter1 = Builders<BsonDocument>.Filter.Eq("walletCode", walletid);
                if (coll1.Find(filter1).Count() > 0)
                {
                    var obj = coll1.Find(filter1).Project(projection1).FirstOrDefault();
                    ase = BsonSerializer.Deserialize<walletForm>(obj);
                    break;
                }
            }

            if (ase.walletCode == null)
            {
                ViewData["Title"] = "All active transactions";
            } else
            {
                ViewData["Title"] = "Transactions of Wallet Code " + ase.walletCode;
            }
            return View(ase);
        }

        [HttpGet("home/TransactionDetail/{transid}")]
        public IActionResult TransactionDetail([FromRoute] string transid = "")
        {
            transaction setobj = new transaction();
            for (int i = 1; i < 4; i++)
            {
                MongoClient dbclient = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{new Zoneselect().zoneselection(i).Split(",")[0]}.mongodb.net/");
                var db = dbclient.GetDatabase("main");
                var coll = db.GetCollection<BsonDocument>("transaction");
                var projection = Builders<BsonDocument>.Projection.Exclude("removeEnt").Exclude("_id");
                var filter = Builders<BsonDocument>.Filter.Eq("transactionId", transid);

                var aa = coll.Find(filter).Project(projection).FirstOrDefault();
                if (aa != null)
                {
                    setobj = BsonSerializer.Deserialize<transaction>(aa);
                    if (_cache.GetAsync<int>("xtrans-" + setobj.transactionId).Result == 0)
                    {
                        bool signx = new Zoneselect().listVerify(setobj.transactionId);
                        _cache.Add("xtrans-" + setobj.transactionId, signx == true ? 2 : 1, DateTimeOffset.Now.AddMinutes(1));
                        setobj.signed = signx;
                    }
                    else
                    {
                        setobj.signed = _cache.GetAsync<int>("xtrans-" + setobj.transactionId).Result == 2 ? true : false;
                    }
                    break;
                }
            }
           

            if (setobj.transactionId == null)
            {
                return RedirectToAction("TransactionList");
            }

            ViewData["Title"] = "Transaction of " + setobj.transactionId;
            return View(setobj);
        }

        public IActionResult TransList([FromQuery] string? walletCode)
        {
            List<transaction> setobj = new List<transaction>();

            walletForm ase = new walletForm();
            for (int i = 1; i < 4; i++)
            {
                MongoClient dbclient1 = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{new Zoneselect().zoneselection(i).Split(",")[0]}.mongodb.net/");
                var db1 = dbclient1.GetDatabase("main");
                var coll1 = db1.GetCollection<BsonDocument>("user");
                var projection1 = Builders<BsonDocument>.Projection.Exclude("_id");
                var filter1 = Builders<BsonDocument>.Filter.Eq("walletCode", walletCode);
                if (coll1.Find(filter1).Count() > 0)
                {
                    var obj = coll1.Find(filter1).Project(projection1).FirstOrDefault();
                    ase = BsonSerializer.Deserialize<walletForm>(obj);
                    break;
                }
            }

            if (walletCode == null || walletCode == "")
            {
                for (int i = 1; i < 4; i++)
                {
                    MongoClient dbclients = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{new Zoneselect().zoneselection(i).Split(",")[0]}.mongodb.net/");
                    var dbs = dbclients.GetDatabase("main");
                    var colls = dbs.GetCollection<BsonDocument>("transaction");
                    var projections = Builders<BsonDocument>.Projection.Exclude("removeEnt").Exclude("_id");
                    foreach (var item in colls.Find(new BsonDocument()).Project(projections).Sort(Builders<BsonDocument>.Sort.Descending("dateChanged")).ToList())
                    {
                        transaction o = BsonSerializer.Deserialize<transaction>(item);
                        if (_cache.GetAsync<int>("trans-" + o.transactionId).Result == 0)
                        {
                            bool signx = new Zoneselect().listVerify(o.transactionId);
                            _cache.Add("trans-" + o.transactionId, signx == true ? 2 : 1, signx == true ? DateTimeOffset.Now.AddMinutes(30) : DateTimeOffset.Now.AddMinutes(1));
                            o.signed = signx;
                        }
                        else
                        {
                            o.signed = _cache.GetAsync<int>("trans-" + o.transactionId).Result == 2 ? true : false;
                        }
                        if (setobj.FindIndex(x => x.transactionId == o.transactionId) == -1)
                        {
                            setobj.Add(o);
                        }
                    }
                }

                if (setobj.Count == 0)
                {
                    return new JsonResult(setobj);
                }

                setobj = setobj.OrderByDescending(x => x.dateChanged).ToList();
                return new JsonResult(setobj);
            }

            for (int i = 1; i < 4; i++)
            {
                MongoClient dbclient1 = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{new Zoneselect().zoneselection(i).Split(",")[0]}.mongodb.net/");
                var db1 = dbclient1.GetDatabase("main");
                var coll1 = db1.GetCollection<BsonDocument>("user");
                var projection1 = Builders<BsonDocument>.Projection.Exclude("_id");
                var filter1 = Builders<BsonDocument>.Filter.Eq("walletCode", walletCode);
                if (coll1.Find(filter1).Count() > 0)
                {
                    var obj = coll1.Find(filter1).Project(projection1).FirstOrDefault();
                    ase = BsonSerializer.Deserialize<walletForm>(obj);
                    break;
                }
            }
            if (ase.walletCode == null)
            {
                return new JsonResult(setobj);
            }

            for (int i = 1; i < 4; i++)
            {
                MongoClient dbclient = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{new Zoneselect().zoneselection(i).Split(",")[0]}.mongodb.net/");
                var db = dbclient.GetDatabase("main");
                var coll = db.GetCollection<BsonDocument>("transaction");
                var projection = Builders<BsonDocument>.Projection.Exclude("removeEnt").Exclude("_id");
                var filter = Builders<BsonDocument>.Filter.Eq("fromWalletCode", walletCode) | Builders<BsonDocument>.Filter.Eq("toWalletCode", walletCode);
                foreach (var item in coll.Find(filter).Project(projection).Sort(Builders<BsonDocument>.Sort.Descending("dateChanged")).ToList())
                {
                    transaction o = BsonSerializer.Deserialize<transaction>(item);
                    if (_cache.GetAsync<int>("trans-" + o.transactionId).Result == 0)
                    {
                        bool signx = new Zoneselect().listVerify(o.transactionId);
                        _cache.Add("trans-" + o.transactionId, signx == true ? 2 : 1, signx == true ? DateTimeOffset.Now.AddMinutes(30) : DateTimeOffset.Now.AddMinutes(1));
                        o.signed = signx;
                    }
                    else
                    {
                        o.signed = _cache.GetAsync<int>("trans-" + o.transactionId).Result == 2 ? true : false;
                    }
                    if (setobj.FindIndex(x => x.transactionId == o.transactionId) == -1)
                    {
                        setobj.Add(o);
                    }
                }
            }

            if (setobj.Count == 0)
            {
                return new JsonResult(setobj);
            }

            setobj = setobj.OrderByDescending(x => x.dateChanged).ToList();
            return new JsonResult(setobj);
        }
    }
}
