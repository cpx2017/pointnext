﻿using ByteDev.Encoding.Base32;
using ByteDev.Encoding.Base64;
using cpxdev_crosspointmangement.Model;
using LazyCache;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Drawing;
using System.Security.Cryptography;
using System.Text;
using static System.Net.Mime.MediaTypeNames;

namespace cpxdev_crosspointmangement
{
    public class Zoneselect
    {
        public string zoneselection(int zone)
        {
            switch (zone)
            {
                case 1:
                    return "cpointzone1.6pzxs6k,Zone1,AS";
                case 2:
                    return "cpointzone2.3tc9dfs,Zone2,EU";
                case 3:
                    return "cpointzone3.ld9430y,Zone3,US";
                default:
                    return "";
            }
        }
        public string MD5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            //compute hash from the bytes of text  
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));


            //get hash result after compute it  
            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits  
                //for each byte  
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }

        public object sendLogNonSign(transactionForm obj, int zone = 0)
        {
            MongoClient dbclient = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{zoneselection(zone).Split(",")[0]}.mongodb.net/");
            var db = dbclient.GetDatabase("main");
            var coll = db.GetCollection<BsonDocument>("transaction");
            coll.InsertOne(obj.ToBsonDocument());
            return obj;
        }
        public void sendLogwithSign(transactionForm obj, int zone)
        {
            for (int i = 1; i < 4; i++)
            {
                if (zone != i)
                {
                    MongoClient dbclient = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{zoneselection(i).Split(",")[0]}.mongodb.net/");
                    var db = dbclient.GetDatabase("main");
                    var coll = db.GetCollection<BsonDocument>("transaction");
                    coll.InsertOne(obj.ToBsonDocument());
                }
            }
        }
  
        public bool listVerify(string transaction)
        {
            int ii = 0;
            for (int i = 1; i < 4; i++)
            {
                MongoClient dbclient = new MongoClient($"mongodb+srv://devzone:5844277072Cnt@{zoneselection(i).Split(",")[0]}.mongodb.net/");
                var db = dbclient.GetDatabase("main");
                var coll = db.GetCollection<BsonDocument>("transaction");
                var filter1 = Builders<BsonDocument>.Filter.Eq("transactionId", transaction);
                if (coll.Find(filter1).Count() > 0)
                {
                    ii += 1;
                }
            }
            if (ii == 3)
            {
                return true;
            }
            return false;
        }
    }
}
