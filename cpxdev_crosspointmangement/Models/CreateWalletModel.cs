﻿
using static System.Net.Mime.MediaTypeNames;

namespace cpxdev_crosspointmangement.Model
{
    public class CreateWalletModel
    {
        public string passKey { get; set; }
        public bool receivedWallet { get; set; }
        public int zone { get; set; }
    }

    public class DeleteWalletModel
    {
        public string walletCode { get; set; }
        public string passKey { get; set; }
    }

    public class Zoning
    {
        public int zoneNumber { get; set; } = 0;
        public string regionZone { get; set; } = "";
    }
    public class transactionForm
    {
        public string fromWalletCode { get; set; } = "";
        public string toWalletCode { get; set; } = "";
        public string transactionId { get; set; }
        public double pointEarn { get; set; } = 0;
        public double pointFee { get; set; } = 0;
        public DateTime dateChanged { get; set; } = DateTime.UtcNow;
        public DateTime removeEnt { get; set; }
        public Zoning from { get; set; }
        public Zoning to { get; set; }
        public string text { get; set; }
    }
    public class walletForm
    {
        public string walletCode { get; set; }
        public string passKey { get; set; }
        public bool receivedWallet { get; set; }
        public DateTime createdDate { get; set; }
        public double pointState { get; set; }
        public int zoneNumber { get; set; }
        public string regionZone { get; set; }
    }
    public class transaction
    {
        public string fromWalletCode { get; set; } = "";
        public string toWalletCode { get; set; } = "";
        public string transactionId { get; set; }
        public double pointEarn { get; set; } = 0;
        public double pointFee { get; set; } = 0;
        public DateTime dateChanged { get; set; } = DateTime.UtcNow;
        public bool signed { get; set; }
        public Zoning from { get; set; }
        public Zoning to { get; set; }
        public string text { get; set; }
    }
    public class CreateTransaction
    {
        public string fromWalletCode { get; set; } = "";
        public string toWalletCode { get; set; } = "";
        public double point { get; set; } = 0;
        public double feePoint { get; set; } = 0;
        public bool autoSignedTransaction { get; set; } = false;
    }
    public class DestroyTransaction
    {
        public bool autoSignedTransaction { get; set; } = false;
    }
    public class CreateTransactionWithThird
    {
        public string fromWalletCode { get; set; } = "";
        public string toWalletName { get; set; } = "";
        public double point { get; set; } = 0;
        public double feePoint { get; set; } = 0;
    }
    public class ReceivedPointTransactionWithThird
    {
        public string fromWalletName { get; set; } = "";
        public string toWalletCode { get; set; } = "";
        public double point { get; set; } = 0;
    }
}
